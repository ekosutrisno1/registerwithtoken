$(() => {
  get_all_rencana();
  $('#page_sorting2').hide();
});

function get_all_rencana() {
  $("#data_rows").html(
    /*html*/
    `<tr> <td colspan="4" style="text-align:center"><i>Loading...</i> </td></td></tr>`
  );
  $.ajax({
    url: "/rest/mahasiswa/page?size=10",
    type: "get",
    contentType: "application/json",
    success: function (result) {

      $("#total-data").text(result.totalElements);
      $("#total-page").text(result.size);
      $("#show-info").text(result.size);
      $("#total-size").text(result.totalPages);
      $("#total-ke").text(result.pageable.pageNumber);
      $("#count-page").val(result.pageable.pageNumber);
      $("#count-first").val(result.first);
      $("#count-last").val(result.last);

      var data = result.content;
      $("#data_rows").html("");
      if (data.length > 0) {
        for (let i = 0; i < data.length; i++) {
          $("#data_rows").append(
            /*html*/
            `<tr>
								<td>
											<div class="row font-weight-bold">
												<div class="col-sm">${data[i].kodeMahasiswa}</div>
												<div class="col-sm">${data[i].namaMahasiswa}</div>
												<div class="col-sm">${data[i].kodeJurusan.kodeJurusan} </div>
									
                        <div class="col-sm"><h5>${data[i].nomorHanphone}</h5></div>
												<div class="col-sm">
                         <h5 class="">
                          <a class="mr-2" data-toggle="modal" data-target="#detail-undangan" href="#"
                              id="showAddData"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                          <a onclick="get_data_byid(${data[i].id},'ubah')"  class="mr-2 " data-toggle="modal" data-target="#modal-rencana" href="#"
                              id="showAddData"><i class="fa fa-edit" aria-hidden="true"></i></a>
                          <a onclick="hapus(${data[i].id})"  class="mr-2" data-toggle="modal" data-target="" href="#"
                              id="showAddData"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </h5>
												</div>
											</div>
								</td>
              </tr>`);
        }
      } else {
        $("#data_rows").html(
          /*html*/
          `<tr> <td colspan="4">
					<div class="alert alert-primary text-center" role="alert">
						Data tidak ditemukan! <span><i class="fa fa-error"></i></span>
					</div> 
					</td></tr>`
        );
      }
    },
    error: function () {
      Swal.fire("", "Failed to fetch data", "error");
    }
  });
};