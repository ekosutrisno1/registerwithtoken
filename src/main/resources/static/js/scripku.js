function getElById(objId) {
  return document.getElementById(objId);
}

function getElByClass(objClass) {
  return document.getElementsByClassName(objClass);
}


function myFunction() {
  let token = document.getElementById("token");
  token.value = token.value.toUpperCase();
}

function cekKoneksi() {
  let connect = navigator.onLine;
  if (connect == false) {
    $('#btn-kirim').attr('disabled', true);
    $(".in").text("Ups.. kamu Offline");
  }
}

function cekKoneksiSubmit() {
  let connect = navigator.onLine;
  if (connect == false) {

    $('#btn-submit').attr('disabled', true);
    $('.un').text("Ups.. kamu Offline");
  }
}