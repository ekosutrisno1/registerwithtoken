package app.principal;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import app.model.Account;
import app.service.account.AccountService;

/**
 * CustomUserDetailService
 */
@Service
public class CustomUserDetailService implements UserDetailsService {

  @Autowired
  AccountService accountService;

  @Override
  @Transactional
  public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
    Account user = accountService.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail).orElseThrow(
        () -> new UsernameNotFoundException("User tidak ditemukan dengan nama atau email : " + usernameOrEmail));

    if (user != null && user.isActive()) {
      return UserPrincipal.create(user);
    }

    throw new UsernameNotFoundException("User '" + usernameOrEmail + "' tidak ditemukan");

  }

}