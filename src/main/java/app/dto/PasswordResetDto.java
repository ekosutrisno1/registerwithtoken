package app.dto;

import javax.validation.constraints.NotEmpty;

import app.validator.field_match.FieldMatch;

/**
 * PasswordResetDto
 */
@FieldMatch(pertama = "password", kedua = "confirmPassword", message = "Password Harus Sama")
public class PasswordResetDto {

  @NotEmpty(message = "Password tidak boleh kosong")
  private String password;

  @NotEmpty(message = "Password tidak boleh kosong")
  private String confirmPassword;

  @NotEmpty(message = "Up kamu tidak punya tiken, ILEGAL")
  private String token;

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getConfirmPassword() {
    return confirmPassword;
  }

  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

}