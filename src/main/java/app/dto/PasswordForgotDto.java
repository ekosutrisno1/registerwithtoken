package app.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * PasswordForgotDto
 */
public class PasswordForgotDto {

  @Email(message = "Email tidak valid, silahkan isi sesuai format email yang benar.")
  @NotEmpty(message = "Upss.. kamu sepertinya lupa mengisi email.")
  private String email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

}