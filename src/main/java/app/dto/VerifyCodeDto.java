package app.dto;

import app.validator.account_verify.ValidVerifyCode;

/**
 * VerifyCodeDto
 */
public class VerifyCodeDto {

  @ValidVerifyCode
  private String token;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}