package app.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * ResendCodeDto
 */
public class ResendCodeDto {

  @Email(message = "Email tidak valid")
  @NotEmpty(message = "Upss.. Kamu amnesia mengisi email")
  private String email;

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

}