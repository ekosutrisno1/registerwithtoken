package app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.model.Account;

/**
 * AccountRepository
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

  Optional<Account> findByUsernameOrEmail(String username, String email);

  Optional<Account> findByEmail(String email);

  Optional<Account> findByUsername(String username);

  @Query(value = "SELECT * FROM users WHERE email = ?", nativeQuery = true)
  Account findByEmailOne(String email);

  String MY_UPDATE = "update Account u set u.password = :password where u.id = :id";

  @Modifying
  @Query(MY_UPDATE)
  void updatePassword(@Param("password") String password, @Param("id") Long id);

}