package app.repository;

import app.model.transaksi.Mahasiswa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MahasiswaRepository extends JpaRepository<Mahasiswa, Long> {

  static final String QUERY_DATA = "SELECT * FROM t_mahasiswa WHERE isdelete = false ORDER BY id ASC";

  @Query(value = QUERY_DATA, nativeQuery = true)
  Page<Mahasiswa> getPaging(Pageable pageable);

}
