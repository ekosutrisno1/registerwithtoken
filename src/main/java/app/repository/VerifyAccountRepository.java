package app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import app.model.VerifyAccount;

/**
 * VerifyAccountRepository
 */
@Repository
public interface VerifyAccountRepository extends JpaRepository<VerifyAccount, Long> {

  Optional<VerifyAccount> findByToken(String token);

  String MY_UPDATE = "update VerifyAccount u set u.token = :token where u.account = :id";

  @Modifying
  @Query(MY_UPDATE)
  void updateToken(@Param("token") String token, @Param("id") Long userId);

  String FIND_USER = "SELECT * FROM verify_account WHERE account_id = ?";

  @Query(value = FIND_USER, nativeQuery = true)
  VerifyAccount findByAccount(Long id);

}