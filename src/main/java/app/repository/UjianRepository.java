package app.repository;

import app.model.master.Ujian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UjianRepository extends JpaRepository<Ujian, Long> {
}
