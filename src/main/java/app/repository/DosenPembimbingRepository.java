package app.repository;

import app.model.transaksi.DosenPembimbing;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * DosenPembimbingRepository
 */
@Repository
public interface DosenPembimbingRepository extends JpaRepository<DosenPembimbing, Long> {
  static final String MY_QUERY = "SELECT * FROM t_dosen_pembimbing WHERE isdelete = false";

  @Query(value = MY_QUERY, nativeQuery = true)
  Page<DosenPembimbing> getAllPaging(Pageable pageable);
}