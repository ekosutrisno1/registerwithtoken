package app.repository;

import app.model.transaksi.Nilai;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface NilaiRepository extends JpaRepository<Nilai, Long> {

  static final String QUERY_PAGING = "SELECT * FROM t_nilai WHERE isdelete = false";

  @Query(value = QUERY_PAGING, nativeQuery = true)
  Page<Nilai> pagination(Pageable pageable);

}
