package app.repository;

import app.model.master.TypeDosen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TypeDosenRepository extends JpaRepository<TypeDosen, Long> {
}
