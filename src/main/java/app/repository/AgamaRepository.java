package app.repository;

import app.model.master.Agama;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgamaRepository extends JpaRepository<Agama, Long> {
   String MY_QUERY = "SELECT * FROM master.agama";

   @Query(value = MY_QUERY, nativeQuery = true)
   List<Agama> findAllData();
}
