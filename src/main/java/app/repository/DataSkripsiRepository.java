package app.repository;

import app.model.transaksi.DataSkripsi;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * DataSkripsiRepository
 */
@Repository
public interface DataSkripsiRepository extends JpaRepository<DataSkripsi, Long> {
  static final String MY_QUERY = "SELECT * FROM t_data_skripsi WHERE isdelete = false";

  @Query(value = MY_QUERY, nativeQuery = true)
  Page<DataSkripsi> getAllPaging(Pageable pageable);

}