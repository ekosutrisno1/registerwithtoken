package app.repository;

import app.model.transaksi.Dosen;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DosenRepository extends JpaRepository<Dosen, Long> {

  static final String QUERI_DATA = "SELECT * FROM t_dosen WHERE isdelete = false";

  @Query(value = QUERI_DATA, nativeQuery = true)
  Page<Dosen> getPaging(Pageable pageable);
}
