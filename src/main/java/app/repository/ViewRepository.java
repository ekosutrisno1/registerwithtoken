package app.repository;

import java.util.List;

import app.model.view.ViewNilai;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 * ViewRepository
 */
@Repository
public interface ViewRepository extends JpaRepository<ViewNilai, Long> {
  static final String QUERY_V1 = "SELECT * FROM get_nilai WHERE isdelete = false ORDER BY id ASC";

  @Query(value = QUERY_V1, nativeQuery = true)
  List<ViewNilai> getAllNilaiView();
}