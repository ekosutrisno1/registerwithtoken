package app.dao.master;

import app.dao.IOperations;
import app.model.master.Agama;

/**
 * AgamaDao
 */
public interface AgamaDao extends IOperations<Agama> {

}