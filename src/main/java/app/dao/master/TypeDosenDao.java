package app.dao.master;

import app.dao.IOperations;
import app.model.master.TypeDosen;

/**
 * TypeDosenDao
 */
public interface TypeDosenDao extends IOperations<TypeDosen> {

}