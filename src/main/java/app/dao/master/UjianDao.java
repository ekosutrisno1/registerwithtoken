package app.dao.master;

import app.dao.IOperations;
import app.model.master.Ujian;

/**
 * UjianDao
 */
public interface UjianDao extends IOperations<Ujian> {

}