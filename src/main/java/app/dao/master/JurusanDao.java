package app.dao.master;

import app.dao.IOperations;
import app.model.master.Jurusan;

/**
 * JurusanDao
 */
public interface JurusanDao extends IOperations<Jurusan> {

}