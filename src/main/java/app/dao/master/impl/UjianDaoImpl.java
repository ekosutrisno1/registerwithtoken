package app.dao.master.impl;

import java.util.List;
import java.util.Optional;

import app.repository.UjianRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.master.UjianDao;
import app.model.master.Ujian;

/**
 * UjianDaoImpl
 */
@Repository
public class UjianDaoImpl implements UjianDao {

  @Autowired
  private UjianRepository ujianREpo;

  @Override
  public Optional<Ujian> findById(Long id) {
    return ujianREpo.findById(id);
  }

  @Override
  public List<Ujian> findAll() {
    return ujianREpo.findAll();
  }

  @Override
  public Ujian create(Ujian ujian) {
    return ujianREpo.save(ujian);
  }

  @Override
  public Ujian update(Ujian ujian) {
    return ujianREpo.save(ujian);
  }

  @Override
  public void delete(Ujian ujian) {
    ujianREpo.delete(ujian);

  }

  @Override
  public void deleteById(long entityId) {
    ujianREpo.deleteById(entityId);

  }

}