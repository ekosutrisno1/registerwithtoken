package app.dao.master.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.master.TypeDosenDao;
import app.model.master.TypeDosen;
import app.repository.TypeDosenRepository;

/**
 * TypeDosenDaoImpl
 */
@Repository
public class TypeDosenDaoImpl implements TypeDosenDao {

  @Autowired
  private TypeDosenRepository typeRepo;

  @Override
  public Optional<TypeDosen> findById(Long id) {
    return typeRepo.findById(id);
  }

  @Override
  public List<TypeDosen> findAll() {
    return typeRepo.findAll();
  }

  @Override
  public TypeDosen create(TypeDosen typeDosen) {
    return typeRepo.save(typeDosen);
  }

  @Override
  public TypeDosen update(TypeDosen typeDosen) {
    return typeRepo.save(typeDosen);
  }

  @Override
  public void delete(TypeDosen typeDosen) {
    typeRepo.delete(typeDosen);

  }

  @Override
  public void deleteById(long tDosenId) {
    typeRepo.deleteById(tDosenId);
  }

}