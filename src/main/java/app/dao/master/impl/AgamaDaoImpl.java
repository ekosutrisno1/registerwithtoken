package app.dao.master.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.master.AgamaDao;
import app.model.master.Agama;
import app.repository.AgamaRepository;

/**
 * AgamaDaoImpl
 */
@Repository
public class AgamaDaoImpl implements AgamaDao {

  @Autowired
  private AgamaRepository agamaRepository;

  @Override
  public Optional<Agama> findById(Long id) {
    return agamaRepository.findById(id);
  }

  @Override
  public List<Agama> findAll() {
    return agamaRepository.findAll();
  }

  @Override
  public Agama create(Agama agama) {
    return agamaRepository.save(agama);
  }

  @Override
  public Agama update(Agama agama) {
    return agamaRepository.save(agama);
  }

  @Override
  public void delete(Agama agama) {
    agamaRepository.delete(agama);
  }

  @Override
  public void deleteById(long agamaId) {
    agamaRepository.deleteById(agamaId);
  }

}