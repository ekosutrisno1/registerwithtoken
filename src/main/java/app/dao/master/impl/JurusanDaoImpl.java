package app.dao.master.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.master.JurusanDao;
import app.model.master.Jurusan;
import app.repository.JurusanRepository;

/**
 * JurusanDaoImpl
 */
@Repository
public class JurusanDaoImpl implements JurusanDao {

  @Autowired
  private JurusanRepository jRepository;

  @Override
  public Optional<Jurusan> findById(Long id) {
    return jRepository.findById(id);
  }

  @Override
  public List<Jurusan> findAll() {
    return jRepository.findAll();
  }

  @Override
  public Jurusan create(Jurusan jurusan) {
    return jRepository.save(jurusan);
  }

  @Override
  public Jurusan update(Jurusan jurusan) {
    return jRepository.save(jurusan);
  }

  @Override
  public void delete(Jurusan jurusan) {
    jRepository.delete(jurusan);

  }

  @Override
  public void deleteById(long jurusanId) {
    jRepository.deleteById(jurusanId);

  }

}