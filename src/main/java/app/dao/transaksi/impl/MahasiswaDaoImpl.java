package app.dao.transaksi.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import app.dao.transaksi.MahasiswaDao;
import app.model.transaksi.Mahasiswa;
import app.repository.MahasiswaRepository;

/**
 * MahasiswaDaoImpl
 */
@Repository
public class MahasiswaDaoImpl implements MahasiswaDao {

  @Autowired
  private MahasiswaRepository mahasiswaRepository;

  @Override
  public Optional<Mahasiswa> findById(Long id) {
    return mahasiswaRepository.findById(id);
  }

  @Override
  public List<Mahasiswa> findAll() {
    return mahasiswaRepository.findAll();
  }

  @Override
  @Transactional
  public Mahasiswa create(Mahasiswa mahasiswa) {
    return mahasiswaRepository.save(mahasiswa);
  }

  @Override
  @Transactional
  public Mahasiswa update(Mahasiswa mahasiswa) {
    return mahasiswaRepository.save(mahasiswa);
  }

  @Override
  @Transactional
  public void delete(Mahasiswa mahasiswa) {
    mahasiswaRepository.delete(mahasiswa);

  }

  @Override
  @Transactional
  public void deleteById(long mahasiswaId) {
    mahasiswaRepository.deleteById(mahasiswaId);

  }

  @Override
  @Transactional
  public Page<Mahasiswa> getPaging(Pageable pageable) {
    return mahasiswaRepository.getPaging(pageable);
  }

}