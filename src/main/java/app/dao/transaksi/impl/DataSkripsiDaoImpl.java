package app.dao.transaksi.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import app.dao.transaksi.DataSkripsiDao;
import app.model.transaksi.DataSkripsi;
import app.repository.DataSkripsiRepository;

/**
 * DataSkripsiDaoImpl
 */
@Repository
public class DataSkripsiDaoImpl implements DataSkripsiDao {

  @Autowired
  private DataSkripsiRepository dataSkripsi;

  @Override
  public Optional<DataSkripsi> findById(Long id) {
    return dataSkripsi.findById(id);
  }

  @Override
  public List<DataSkripsi> findAll() {
    return dataSkripsi.findAll();
  }

  @Override
  @Transactional
  public DataSkripsi create(DataSkripsi dataSkrips) {
    return dataSkripsi.save(dataSkrips);
  }

  @Override
  @Transactional
  public DataSkripsi update(DataSkripsi dataSkrips) {
    return dataSkripsi.save(dataSkrips);
  }

  @Override
  @Transactional
  public void delete(DataSkripsi dataSkrips) {
    dataSkripsi.delete(dataSkrips);

  }

  @Override
  @Transactional
  public void deleteById(long dSkrpsiId) {
    dataSkripsi.deleteById(dSkrpsiId);

  }

  @Override
  @Transactional
  public Page<DataSkripsi> getAllPaging(Pageable pageable) {
    return dataSkripsi.getAllPaging(pageable);
  }

}