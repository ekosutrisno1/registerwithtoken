package app.dao.transaksi.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import app.dao.transaksi.DosenDao;
import app.model.transaksi.Dosen;
import app.repository.DosenRepository;

/**
 * DosenDaoImpl
 */
@Repository
public class DosenDaoImpl implements DosenDao {

  @Autowired
  private DosenRepository dosenRepo;

  @Override
  public Optional<Dosen> findById(Long id) {
    return dosenRepo.findById(id);
  }

  @Override
  public List<Dosen> findAll() {
    return dosenRepo.findAll();
  }

  @Override
  @Transactional
  public Dosen create(Dosen dosen) {
    return dosenRepo.save(dosen);
  }

  @Override
  @Transactional
  public Dosen update(Dosen dosen) {
    return dosenRepo.save(dosen);
  }

  @Override
  @Transactional
  public void delete(Dosen dosen) {
    dosenRepo.delete(dosen);

  }

  @Override
  @Transactional
  public void deleteById(long dosenId) {
    dosenRepo.deleteById(dosenId);

  }

  @Override
  @Transactional
  public Page<Dosen> getPaging(Pageable pageable) {
    return dosenRepo.findAll(pageable);
  }

}