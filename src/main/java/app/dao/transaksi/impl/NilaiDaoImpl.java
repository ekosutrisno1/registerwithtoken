package app.dao.transaksi.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import app.dao.transaksi.NilaiDao;
import app.model.transaksi.Nilai;
import app.repository.NilaiRepository;

/**
 * NilaiDaoImpl
 */
@Repository
public class NilaiDaoImpl implements NilaiDao {

  @Autowired
  private NilaiRepository nilaiRepository;

  @Override
  public Optional<Nilai> findById(Long id) {
    return nilaiRepository.findById(id);
  }

  @Override
  public List<Nilai> findAll() {
    return nilaiRepository.findAll();
  }

  @Override
  @Transactional
  public Nilai create(Nilai entity) {
    return nilaiRepository.save(entity);
  }

  @Override
  @Transactional
  public Nilai update(Nilai entity) {
    return nilaiRepository.save(entity);
  }

  @Override
  @Transactional
  public void delete(Nilai entity) {
    nilaiRepository.delete(entity);

  }

  @Override
  @Transactional
  public void deleteById(long nilaiId) {
    nilaiRepository.deleteById(nilaiId);
  }

  @Override
  @Transactional
  public Page<Nilai> pagination(Pageable pageable) {
    return nilaiRepository.pagination(pageable);
  }

}