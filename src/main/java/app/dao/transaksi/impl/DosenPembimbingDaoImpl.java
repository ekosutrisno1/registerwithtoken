package app.dao.transaksi.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import app.dao.transaksi.DosenPembimbingDao;
import app.model.transaksi.DosenPembimbing;
import app.repository.DosenPembimbingRepository;

/**
 * DosenPembimbingDaoImpl
 */
@Repository
public class DosenPembimbingDaoImpl implements DosenPembimbingDao {

  @Autowired
  private DosenPembimbingRepository dosenPbbRepo;

  @Override
  public Optional<DosenPembimbing> findById(Long id) {
    return dosenPbbRepo.findById(id);
  }

  @Override
  public List<DosenPembimbing> findAll() {
    return dosenPbbRepo.findAll();
  }

  @Override
  @Transactional
  public DosenPembimbing create(DosenPembimbing entity) {
    return dosenPbbRepo.save(entity);
  }

  @Override
  @Transactional
  public DosenPembimbing update(DosenPembimbing entity) {
    return dosenPbbRepo.save(entity);
  }

  @Override
  @Transactional
  public void delete(DosenPembimbing entity) {
    dosenPbbRepo.delete(entity);

  }

  @Override
  @Transactional
  public void deleteById(long dosenId) {
    dosenPbbRepo.deleteById(dosenId);

  }

  @Override
  @Transactional
  public Page<DosenPembimbing> getAllPaging(Pageable pageable) {
    return dosenPbbRepo.getAllPaging(pageable);
  }

}