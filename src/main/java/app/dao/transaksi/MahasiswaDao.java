package app.dao.transaksi;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import app.dao.IOperations;
import app.model.transaksi.Mahasiswa;

/**
 * MahasiswaDao
 */
public interface MahasiswaDao extends IOperations<Mahasiswa> {

  Page<Mahasiswa> getPaging(Pageable pageable);
}