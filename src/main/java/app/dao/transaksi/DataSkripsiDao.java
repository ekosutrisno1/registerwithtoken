package app.dao.transaksi;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import app.dao.IOperations;
import app.model.transaksi.DataSkripsi;

/**
 * DataSkripsiDao
 */
public interface DataSkripsiDao extends IOperations<DataSkripsi> {

  Page<DataSkripsi> getAllPaging(Pageable pageable);
}