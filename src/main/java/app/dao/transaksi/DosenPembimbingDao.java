package app.dao.transaksi;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import app.dao.IOperations;
import app.model.transaksi.DosenPembimbing;

/**
 * DosenPembimbingDao
 */
public interface DosenPembimbingDao extends IOperations<DosenPembimbing> {

  Page<DosenPembimbing> getAllPaging(Pageable pageable);
}