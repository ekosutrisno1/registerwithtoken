package app.dao.transaksi;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import app.dao.IOperations;
import app.model.transaksi.Dosen;

/**
 * DosenDao
 */
public interface DosenDao extends IOperations<Dosen> {

  Page<Dosen> getPaging(Pageable pageable);
}