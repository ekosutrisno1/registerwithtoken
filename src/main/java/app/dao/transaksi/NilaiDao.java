package app.dao.transaksi;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import app.dao.IOperations;
import app.model.transaksi.Nilai;

/**
 * NilaiDao
 */
public interface NilaiDao extends IOperations<Nilai> {

  Page<Nilai> pagination(Pageable pageable);
}