package app.dao.view.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.view.ViewNilaiDao;
import app.model.view.ViewNilai;
import app.repository.ViewRepository;

/**
 * ViewNilaiDaoImpl
 */
@Repository
public class ViewNilaiDaoImpl implements ViewNilaiDao {

  @Autowired
  private ViewRepository viewNilaiRepository;

  @Override
  public Optional<ViewNilai> findById(Long id) {
    return viewNilaiRepository.findById(id);
  }

  @Override
  public List<ViewNilai> findAll() {
    return viewNilaiRepository.findAll();
  }

  @Override
  public ViewNilai create(ViewNilai viewNilai) {
    return viewNilaiRepository.save(viewNilai);
  }

  @Override
  public ViewNilai update(ViewNilai viewNilai) {
    return viewNilaiRepository.save(viewNilai);
  }

  @Override
  public void delete(ViewNilai viewNilai) {
    viewNilaiRepository.delete(viewNilai);
  }

  @Override
  public void deleteById(long viewNilaiId) {
    viewNilaiRepository.deleteById(viewNilaiId);

  }

  @Override
  public List<ViewNilai> getAllNilaiView() {
    return viewNilaiRepository.getAllNilaiView();
  }

}