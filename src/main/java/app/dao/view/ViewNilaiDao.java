package app.dao.view;

import java.util.List;

import app.dao.IOperations;
import app.model.view.ViewNilai;

/**
 * ViewNilaiDao
 */
public interface ViewNilaiDao extends IOperations<ViewNilai> {

  List<ViewNilai> getAllNilaiView();
}