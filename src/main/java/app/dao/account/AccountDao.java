package app.dao.account;

import java.util.Optional;

import app.dao.IOperations;
import app.model.Account;

/**
 * AccountDao
 */
public interface AccountDao extends IOperations<Account> {

  Optional<Account> findByUsernameOrEmail(String username, String email);

  Optional<Account> findByEmail(String email);

  Optional<Account> findByUsername(String username);

  Account findByEmailOne(String email);

  void updatePassword(String password, Long id);
}