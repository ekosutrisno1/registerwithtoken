package app.dao.role;

import app.dao.IOperations;
import app.model.Role;

/**
 * RoleDao
 */
public interface RoleDao extends IOperations<Role> {

}