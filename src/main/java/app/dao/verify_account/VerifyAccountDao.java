package app.dao.verify_account;

import java.util.Optional;

import app.model.VerifyAccount;

/**
 * VerifyAccountDao
 */
public interface VerifyAccountDao {

  VerifyAccount create(VerifyAccount verifyAccount);

  VerifyAccount update(VerifyAccount verifyAccount);

  Optional<VerifyAccount> findByToken(String token);

  Optional<VerifyAccount> findById(Long id);

  void updateToken(String token, Long userId);

  VerifyAccount findByAccount(Long id);

  void delete(VerifyAccount id);

}