package app.dao.verify_account.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.verify_account.VerifyAccountDao;
import app.model.VerifyAccount;
import app.repository.VerifyAccountRepository;

/**
 * VerifyAccountDaoImplementation
 */
@Repository
public class VerifyAccountDaoImplementation implements VerifyAccountDao {

  @Autowired
  private VerifyAccountRepository verifyAccountRepository;

  @Override
  public VerifyAccount create(VerifyAccount verifyAccount) {
    return verifyAccountRepository.save(verifyAccount);
  }

  @Override
  public VerifyAccount update(VerifyAccount verifyAccount) {
    return verifyAccountRepository.save(verifyAccount);
  }

  @Override
  public Optional<VerifyAccount> findByToken(String token) {
    return verifyAccountRepository.findByToken(token);
  }

  @Override
  public Optional<VerifyAccount> findById(Long id) {
    return verifyAccountRepository.findById(id);
  }

  @Override
  public void updateToken(String token, Long userId) {
    verifyAccountRepository.updateToken(token, userId);
  }

  @Override
  public VerifyAccount findByAccount(Long id) {
    return verifyAccountRepository.findByAccount(id);
  }

  @Override
  public void delete(VerifyAccount verifyAccount) {
    verifyAccountRepository.delete(verifyAccount);
  }

}