package app.validator.anotasi;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import app.validator.validasi.CreateUsernameValidator;

/**
 * ValidCreateUsername
 */

@Target({ ElementType.TYPE, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CreateUsernameValidator.class)
@Documented
public @interface ValidCreateUsername {

  String message() default "Username tidak valid";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}