package app.validator.anotasi;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import app.validator.validasi.EmailValidators;

/**
 * ValidEmail
 */

@Target({ ElementType.TYPE, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EmailValidators.class)
@Documented
public @interface ValidEmail {
  String message() default "Email tidak valid";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

}