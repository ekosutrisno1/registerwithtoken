package app.validator.account_verify;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import app.dao.verify_account.VerifyAccountDao;
import app.model.VerifyAccount;

/**
 * VerifyCodeValidator
 */
public class VerifyCodeValidator implements ConstraintValidator<ValidVerifyCode, String> {

  @Autowired
  private VerifyAccountDao verifyAccountDao;

  @Override
  public boolean isValid(String token, ConstraintValidatorContext context) {
    if (token.isEmpty()) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate("Kode tidak boleh kosong").addConstraintViolation();
      return false;

    } else if (!verifyAccountDao.findByToken(token).isPresent()) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate("Kode tidak ditemukan").addConstraintViolation();
      return false;

    } else {
      VerifyAccount verifyAccount = verifyAccountDao.findByToken(token).get();

      if (verifyAccount.isExpired()) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate("Kode verifikasi kadaluarsa").addConstraintViolation();
        return false;
      }
    }
    return true;
  }

}