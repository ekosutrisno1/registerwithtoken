package app.validator.field_match;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.beanutils.BeanUtils;

public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {

  private String fieldPertama;
  private String fieldKedua;

  @Override
  public void initialize(final FieldMatch constraintAnnotation) {
    fieldPertama = constraintAnnotation.pertama();
    fieldKedua = constraintAnnotation.kedua();
  }

  @Override
  public boolean isValid(final Object obj, final ConstraintValidatorContext context) {
    try {
      final Object objPertama = BeanUtils.getProperty(obj, fieldPertama);
      final Object objKedua = BeanUtils.getProperty(obj, fieldKedua);
      return objPertama == null && objKedua == null || objPertama != null && objPertama.equals(objKedua);
    } catch (final Exception ignore) {
    }
    return true;
  }

}
