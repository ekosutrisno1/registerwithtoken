package app.validator.validasi;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import app.util.RegexUtil;
import app.validator.anotasi.ValidEmail;

/**
 * EmailValidator
 */
public class EmailValidators implements ConstraintValidator<ValidEmail, String> {
  @Override
  public void initialize(ValidEmail constraintAnnotation) {
    ConstraintValidator.super.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(String email, ConstraintValidatorContext context) {
    if (email.isEmpty()) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate("Kolom email tidak boleh kosong").addConstraintViolation();
      return false;
    }
    if (!RegexUtil.validateEmail(email)) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate("Email tidak valid").addConstraintViolation();
      return false;
    }

    return true;
  }

}