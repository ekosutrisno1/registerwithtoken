package app.validator.validasi;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import app.dto.AccountUpdateDto;
import app.model.Account;
import app.service.account.AccountService;
import app.validator.anotasi.ValidUpdateUsername;

/**
 * UpdateUsernameValidator
 */
public class UpdateUsernameValidator implements ConstraintValidator<ValidUpdateUsername, Object> {

  @Autowired
  private AccountService accountService;

  @Override
  public boolean isValid(Object obj, ConstraintValidatorContext context) {

    if (obj instanceof AccountUpdateDto) {
      AccountUpdateDto dto = (AccountUpdateDto) obj;
      Long id = dto.getId();
      String username = dto.getUsername();

      if (accountService.findById(id).isPresent()) {
        Account account = accountService.findById(id).get();
        if (!account.getUsername().equals(username)) {
          context.disableDefaultConstraintViolation();
          context.buildConstraintViolationWithTemplate("Username sudah terdaftar").addPropertyNode("username")
              .addConstraintViolation();
          return false;
        }
      }
    }
    return true;
  }

}