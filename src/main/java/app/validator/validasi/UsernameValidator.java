package app.validator.validasi;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import app.util.RegexUtil;
import app.validator.anotasi.ValidUsername;

/**
 * UsernameValidator
 */
public class UsernameValidator implements ConstraintValidator<ValidUsername, String> {

  @Override
  public boolean isValid(String username, ConstraintValidatorContext context) {

    if (username.isEmpty()) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate("Field username harus terisi").addConstraintViolation();
      return false;
    }

    if (!RegexUtil.validUsername(username)) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate("Username tidak valid").addConstraintViolation();
      return false;
    }

    return true;
  }

}