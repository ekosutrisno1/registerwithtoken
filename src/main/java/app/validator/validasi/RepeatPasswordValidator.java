package app.validator.validasi;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import app.dto.AccountCreateDto;
import app.dto.AccountUpdateDto;
import app.validator.anotasi.ValidRepeatPassword;

/**
 * RepeatPasswordValidator
 */
public class RepeatPasswordValidator implements ConstraintValidator<ValidRepeatPassword, Object> {

  @Override
  public boolean isValid(Object obj, ConstraintValidatorContext context) {

    if (obj instanceof AccountCreateDto) {
      AccountCreateDto dto = (AccountCreateDto) obj;
      if (dto.getPassword().isEmpty()) {
        context.buildConstraintViolationWithTemplate("Password tidak boleh kosong").addPropertyNode("repeatPassword")
            .addConstraintViolation();

        return false;
      }

      if (!dto.getPassword().equals(dto.getRepeatPassword())) {
        context.buildConstraintViolationWithTemplate("Password tidak cocok").addPropertyNode("repeatPassword")
            .addConstraintViolation();

        return false;
      }

    }

    if (obj instanceof AccountUpdateDto) {

      AccountUpdateDto dto = (AccountUpdateDto) obj;

      if (dto.getPassword().isEmpty()) {
        context.buildConstraintViolationWithTemplate("Password tidak boleh kosong.").addPropertyNode("repeatPassword")
            .addConstraintViolation();
        return false;
      }

      if (!dto.getPassword().equals(dto.getRepeatPassword())) {
        context.buildConstraintViolationWithTemplate("Password tidak sama").addPropertyNode("repeatPassword")
            .addConstraintViolation();
        return false;
      }
    }

    return true;
  }

}
