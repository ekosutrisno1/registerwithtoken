package app.validator.validasi;

import java.util.HashMap;
import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import app.util.RegexUtil;
import app.validator.anotasi.ValidPassword;

/**
 * PasswordValidator
 */
public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {

  @Override
  public boolean isValid(String password, ConstraintValidatorContext context) {
    Map<String, String> maps = new HashMap<String, String>();

    if (!RegexUtil.validatePassword(password, maps)) {
      String errorMesage = maps.get("errorMessage");

      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate(errorMesage).addConstraintViolation();
      return false;
    }
    return true;
  }

}