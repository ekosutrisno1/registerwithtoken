package app.validator.validasi;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import app.service.account.AccountService;
import app.validator.anotasi.ValidCreateUsername;

/**
 * CreateUsernameValidator
 */
public class CreateUsernameValidator implements ConstraintValidator<ValidCreateUsername, String> {

  @Autowired
  private AccountService accountService;

  @Override
  public boolean isValid(String username, ConstraintValidatorContext context) {

    if (accountService.findByUsername(username).isPresent()) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate("Username sudah ada").addConstraintViolation();
      return false;
    }
    return true;
  }

}