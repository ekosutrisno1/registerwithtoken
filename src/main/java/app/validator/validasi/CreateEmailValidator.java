package app.validator.validasi;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import app.service.account.AccountService;
import app.validator.anotasi.ValidCreateEmail;

/**
 * CreateEmailValidator
 */
public class CreateEmailValidator implements ConstraintValidator<ValidCreateEmail, String> {

  @Autowired
  private AccountService accountService;

  @Override
  public void initialize(ValidCreateEmail constraintAnnotation) {
    ConstraintValidator.super.initialize(constraintAnnotation);
  }

  @Override
  public boolean isValid(String email, ConstraintValidatorContext context) {

    if (accountService.findByEmail(email).isPresent()) {
      context.disableDefaultConstraintViolation();
      context.buildConstraintViolationWithTemplate("Email Telah digunakan").addConstraintViolation();
      return false;
    }

    return true;

  }

}