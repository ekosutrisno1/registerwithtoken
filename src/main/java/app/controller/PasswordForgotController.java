package app.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import app.dto.PasswordForgotDto;
import app.mail.Mail;
import app.mail.MailService;
import app.model.Account;
import app.model.PasswordResetToken;
import app.repository.PasswordResetTokenRepository;
import app.service.account.AccountService;

/**
 * PasswordForgotController
 */
@Controller
@RequestMapping("/forgot-password")
public class PasswordForgotController {

  @Autowired
  private AccountService accountService;

  @Autowired
  private PasswordResetTokenRepository passwordResetTokenRepository;

  @Autowired
  private MailService mailService;

  @ModelAttribute("forgotPasswordForm")
  public PasswordForgotDto lupaPasswordDto() {
    return new PasswordForgotDto();
  }

  @GetMapping
  public ModelAndView halamanForgotPassword() {
    ModelAndView data = new ModelAndView("auth/forgot-password");
    String judul = "Exo App | Lupa Password";
    data.addObject("judul", judul);
    return data;
  }

  @PostMapping
  public String forgotPasswordForm(@ModelAttribute("forgotPasswordForm") @Valid PasswordForgotDto form,
      BindingResult result, HttpServletRequest request) throws MessagingException {
    if (result.hasErrors())
      return "auth/forgot-password";

    Account user = accountService.findByEmailOne(form.getEmail());

    if (user == null) {
      result.rejectValue("email", null, "Exo App tidak dapat menemukan akun dengan alamat email tersebut");
      return "auth/forgot-password";
    }

    PasswordResetToken token = new PasswordResetToken();
    token.setToken(UUID.randomUUID().toString());
    token.setUser(user);
    token.setExpiyDate(30);
    passwordResetTokenRepository.save(token);

    Mail mail = new Mail();
    mail.setFrom("xsisappv2.0@gmail.com");
    mail.setSubject("Reset Password Token");
    mail.setTo(user.getEmail());

    Map<String, Object> model = new HashMap<>();
    model.put("token", token);
    model.put("user", user);
    model.put("ttd", "Exo App V.1.0 Team Support");

    String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
    model.put("resetUrl", url + "/reset-password?token=" + token.getToken());
    mail.setModel(model);

    mailService.sendEmailForgotPassword(mail);

    return "redirect:/forgot-password?succes";
  }

}