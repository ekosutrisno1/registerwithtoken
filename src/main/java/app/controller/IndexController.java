package app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * IndexController
 */
@Controller
@RequestMapping("/")
public class IndexController {

  @GetMapping
  public ModelAndView index() {
    ModelAndView data = new ModelAndView("index");
    String judul = "Exo App | Home";
    data.addObject("judul", judul);
    return data;
  }

  @GetMapping("login")
  public ModelAndView login() {
    ModelAndView data = new ModelAndView("auth/login");
    String judul = "Exo App | Login";
    data.addObject("judul", judul);
    return data;
  }

  @GetMapping("about")
  public ModelAndView aboutPage() {
    ModelAndView data = new ModelAndView("views/admin");
    String judul = "Exo App | About";
    data.addObject("judul", judul);
    return data;
  }

  @GetMapping("app/profile")
  public ModelAndView profilePage() {
    ModelAndView data = new ModelAndView("views/profile");
    String judul = "Exo App | Profile";
    data.addObject("judul", judul);
    return data;
  }

  @GetMapping("app/dashboard")
  public ModelAndView dashboarPage() {
    ModelAndView data = new ModelAndView("data");
    String judul = "Exo App | Dashboard";
    String gambar = "/img/default.jpg";
    data.addObject("judul", judul);
    data.addObject("gambar", gambar);
    return data;
  }

  @GetMapping("app/management")
  public ModelAndView mangementPage() {
    ModelAndView data = new ModelAndView("views/management");
    String judul = "Exo App | Management";
    data.addObject("judul", judul);
    return data;
  }

}