package app.controller;

import javax.mail.MessagingException;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import app.dto.AccountCreateDto;
import app.dto.ResendCodeDto;
import app.dto.VerifyCodeDto;
import app.model.Account;
import app.service.account.AccountService;

/**
 * AccountController
 */
@Controller
public class AccountController {

  @Autowired
  private AccountService accountService;

  @GetMapping("sign-up")
  public ModelAndView signUp(AccountCreateDto accountCreateDto, Model model) {
    ModelAndView data = new ModelAndView("auth/sign-up");
    String judul = "Exo App | Registrasi";
    data.addObject("judul", judul);
    return data;
  }

  @PostMapping("sign-up")
  public String signUp(@Valid AccountCreateDto accountCreateDto, BindingResult result) throws Exception {
    if (result.hasErrors())
      return "auth/sign-up";

    Account account = accountService.createMember(accountCreateDto);
    accountCreateDto.setId(account.getId());

    return "redirect:/verify-code";
  }

  @GetMapping("verify-code")
  public ModelAndView verifyCode(Model model, VerifyCodeDto verifyCodeDto, ResendCodeDto resendCodeDto) {
    ModelAndView data = new ModelAndView("auth/verify-code");
    String judul = "Exo App | Verifikasi Kode";
    data.addObject("judul", judul);
    return data;
  }

  @PostMapping("verify-code")
  public String verifyCodeAction(Model model, @Valid VerifyCodeDto verifyCodeDto, @Valid BindingResult result) {
    if (result.hasErrors())
      return "auth/verify-code";

    VerifyCodeDto data = new VerifyCodeDto();
    String token = verifyCodeDto.getToken().toUpperCase();
    data.setToken(token);
    accountService.verifyCode(data);

    return "redirect:/login";
  }

  @GetMapping("resend-code")
  public ModelAndView resendCode(Model model, ResendCodeDto resendCodeDto) {
    ModelAndView data = new ModelAndView("auth/resend-code");
    String judul = "Exo App | Resend Kode";
    data.addObject("judul", judul);
    return data;
  }

  @PostMapping("resend-token")
  public String resendCodeAction(Model model, @Valid ResendCodeDto resendCodeDto, BindingResult result)
      throws MessagingException {
    if (result.hasErrors())
      return "auth/resend-code";

    accountService.resendCode(resendCodeDto);
    return "redirect:/verify-code";
  }

}