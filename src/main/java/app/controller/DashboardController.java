package app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * DashboardController
 */
@Controller
public class DashboardController {

  @GetMapping("app/dashboard/mahasiswa")
  public ModelAndView dataMahasiswaView() {
    ModelAndView mahasiswa = new ModelAndView("dashboard-view/data-mahasiswa");
    String judul = "Exo App | Mahasiswa";
    String gambar = "/img/default.jpg";
    mahasiswa.addObject("judul", judul);
    mahasiswa.addObject("gambar", gambar);
    return mahasiswa;
  }

}