package app.controller;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import app.dto.PasswordResetDto;
import app.model.Account;
import app.model.PasswordResetToken;
import app.repository.PasswordResetTokenRepository;
import app.service.account.AccountService;

/**
 * PasswordResetController
 */
@Controller
@RequestMapping("/reset-password")
public class PasswordResetController {

  @Autowired
  private AccountService accountService;

  @Autowired
  private PasswordResetTokenRepository passwordRepository;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @ModelAttribute("passwordResetForm")
  public PasswordResetDto passwordResetDto() {
    return new PasswordResetDto();
  }

  @GetMapping
  public String formResetPassword(@RequestParam(required = false) String token, Model model) {
    PasswordResetToken resetToken = passwordRepository.findByToken(token);
    String judul = "Exo App | Reset Password";
    model.addAttribute("judul", judul);

    if (resetToken == null) {
      model.addAttribute("error", "Tidak ditemukan Token Reset Password");
    } else if (resetToken.isExpired()) {
      model.addAttribute("error", "Token telah kadaluarsa, silahkan kirim kembali token Reset Pasword");
    } else {
      model.addAttribute("token", resetToken.getToken());
    }
    return "auth/reset-password";
  }

  @PostMapping
  @Transactional
  public String passwordResetProses(@ModelAttribute("passwordResetForm") @Valid PasswordResetDto form,
      BindingResult result, RedirectAttributes redirectAttributes) {
    if (result.hasErrors()) {
      redirectAttributes.addFlashAttribute(BindingResult.class.getName() + ".passwordResetForm", result);
      redirectAttributes.addFlashAttribute("passwordResetForm", form);
      return "redirect:/reset-password?token=" + form.getToken();
    }

    PasswordResetToken token = passwordRepository.findByToken(form.getToken());
    Account user = token.getUser();
    String password = passwordEncoder.encode(form.getPassword());
    accountService.updatePassword(password, user.getId());
    passwordRepository.delete(token);

    return "redirect:/login?resetSuccess";

  }
}