package app.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * PasswordResetToken
 */
@Entity
public class PasswordResetToken {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(nullable = false, unique = true)
  private String token;

  @Column(nullable = false)
  private Date expirDate;

  @OneToOne(targetEntity = Account.class, fetch = FetchType.EAGER)
  @JoinColumn(nullable = false, name = "user_id")
  private Account user;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Date getExpirDate() {
    return expirDate;
  }

  public void setExpirDate(Date expirDate) {
    this.expirDate = expirDate;
  }

  public Account getUser() {
    return user;
  }

  public void setUser(Account user) {
    this.user = user;
  }

  public void setExpiyDate(int menit) {
    Calendar now = Calendar.getInstance();
    now.add(Calendar.MINUTE, menit);
    this.expirDate = now.getTime();
  }

  public boolean isExpired() {
    return new Date().after(this.expirDate);
  }

}