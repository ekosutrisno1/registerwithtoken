package app.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Role
 */
@Entity
@Table(name = Role.NAMA_TABLE)
public class Role implements Serializable {

  private static final long serialVersionUID = 1L;

  public static final String NAMA_TABLE = "roles";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", unique = true, length = 100)
  private String name;

  @Column(name = "description")
  private String decription;

  @ManyToMany(mappedBy = "roles")
  private Set<Account> users = new HashSet<>();

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDecription() {
    return decription;
  }

  public void setDecription(String decription) {
    this.decription = decription;
  }

  public Set<Account> getUsers() {
    return users;
  }

  public void setUsers(Set<Account> users) {
    this.users = users;
  }

}