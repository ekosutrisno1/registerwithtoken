package app.model.transaksi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import app.model.CommonEntity;
import app.model.master.Ujian;

@Entity
@Table(name = Nilai.TABLE_NAME)
public class Nilai extends CommonEntity {
   private static final long serialVersionUID = -6570927792155947575L;

   public static final String TABLE_NAME = "t_nilai";

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Long id;

   @ManyToOne
   @JoinColumn(name = "kode_mahasiswa", nullable = true, referencedColumnName = "id")
   @NotBlank
   private Mahasiswa kodeMahasiswa;

   @ManyToOne
   @JoinColumn(name = "kode_ujian", nullable = true, referencedColumnName = "id")
   @NotBlank
   private Ujian kodeUjian;

   @Column(name = "nilai", length = 5)
   private String nilaiMahasiswa;

   public Nilai() {
   }

   public Nilai(Long id, Mahasiswa kodeMahasiswa, Ujian kodeUjian, String nilaiMahasiswa) {
      this.id = id;
      this.kodeMahasiswa = kodeMahasiswa;
      this.kodeUjian = kodeUjian;
      this.nilaiMahasiswa = nilaiMahasiswa;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Mahasiswa getKodeMahasiswa() {
      return kodeMahasiswa;
   }

   public void setKodeMahasiswa(Mahasiswa kodeMahasiswa) {
      this.kodeMahasiswa = kodeMahasiswa;
   }

   public Ujian getKodeUjian() {
      return kodeUjian;
   }

   public void setKodeUjian(Ujian kodeUjian) {
      this.kodeUjian = kodeUjian;
   }

   public String getNilaiMahasiswa() {
      return nilaiMahasiswa;
   }

   public void setNilaiMahasiswa(String nilaiMahasiswa) {
      this.nilaiMahasiswa = nilaiMahasiswa;
   }

}
