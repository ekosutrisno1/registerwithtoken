package app.model.transaksi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import app.model.CommonEntity;
import app.model.master.Jurusan;
import app.model.master.TypeDosen;

@Entity
@Table(name = Dosen.TABLE_NAME)
public class Dosen extends CommonEntity {
   private static final long serialVersionUID = -2257386492792012140L;

   public static final String TABLE_NAME = "t_dosen";

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Long id;

   @Column(name = "kode_dosen", nullable = true, length = 5)
   @NotBlank
   private String kodeDosen;

   @Column(name = "nama_dosen", nullable = true, length = 100)
   @NotBlank
   private String namaDosen;

   @ManyToOne
   @JoinColumn(name = "kode_jurusan", nullable = false, referencedColumnName = "id")
   private Jurusan kodeJurusan;

   @ManyToOne
   @JoinColumn(name = "kode_type_dosen", nullable = false, referencedColumnName = "id")
   private TypeDosen kodeTypeDosen;

   public Dosen() {
   }

   public Dosen(Long id, String kodeDosen, String namaDosen, Jurusan kodeJurusan, TypeDosen kodeTypeDosen) {
      this.id = id;
      this.kodeDosen = kodeDosen;
      this.namaDosen = namaDosen;
      this.kodeJurusan = kodeJurusan;
      this.kodeTypeDosen = kodeTypeDosen;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getKodeDosen() {
      return kodeDosen;
   }

   public void setKodeDosen(String kodeDosen) {
      this.kodeDosen = kodeDosen;
   }

   public String getNamaDosen() {
      return namaDosen;
   }

   public void setNamaDosen(String namaDosen) {
      this.namaDosen = namaDosen;
   }

   public Jurusan getKodeJurusan() {
      return kodeJurusan;
   }

   public void setKodeJurusan(Jurusan kodeJurusan) {
      this.kodeJurusan = kodeJurusan;
   }

   public TypeDosen getKodeTypeDosen() {
      return kodeTypeDosen;
   }

   public void setKodeTypeDosen(TypeDosen kodeTypeDosen) {
      this.kodeTypeDosen = kodeTypeDosen;
   }

}
