package app.model.transaksi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import app.model.CommonEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * DataSkripsi
 */
@Entity
@Table(name = DataSkripsi.NAMA_TABLE)
public class DataSkripsi extends CommonEntity {
  private static final long serialVersionUID = -4574821185080358447L;

  public static final String NAMA_TABLE = "t_data_skripsi";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "kode_mahasiswa", nullable = false)
  private Mahasiswa kodeMahasiswa;

  @Column(name = "judul_skripsi")
  private String judulSkripsi;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+07:00")
  @Column(name = "tanggal_mulai")
  private Date tanggalMulai;

  public DataSkripsi() {
  }

  public DataSkripsi(Long id, Mahasiswa kodeMahasiswa, String judulSkripsi, Date tanggalMulai) {
    this.id = id;
    this.kodeMahasiswa = kodeMahasiswa;
    this.judulSkripsi = judulSkripsi;
    this.tanggalMulai = tanggalMulai;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Mahasiswa getKodeMahasiswa() {
    return this.kodeMahasiswa;
  }

  public void setKodeMahasiswa(Mahasiswa kodeMahasiswa) {
    this.kodeMahasiswa = kodeMahasiswa;
  }

  public String getJudulSkripsi() {
    return this.judulSkripsi;
  }

  public void setJudulSkripsi(String judulSkripsi) {
    this.judulSkripsi = judulSkripsi;
  }

  public Date getTanggalMulai() {
    return this.tanggalMulai;
  }

  public void setTanggalMulai(Date tanggalMulai) {
    this.tanggalMulai = tanggalMulai;
  }

  public DataSkripsi id(Long id) {
    this.id = id;
    return this;
  }

  public DataSkripsi kodeMahasiswa(Mahasiswa kodeMahasiswa) {
    this.kodeMahasiswa = kodeMahasiswa;
    return this;
  }

  public DataSkripsi judulSkripsi(String judulSkripsi) {
    this.judulSkripsi = judulSkripsi;
    return this;
  }

  public DataSkripsi tanggalMulai(Date tanggalMulai) {
    this.tanggalMulai = tanggalMulai;
    return this;
  }

}