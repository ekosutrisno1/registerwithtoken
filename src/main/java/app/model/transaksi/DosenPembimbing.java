package app.model.transaksi;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import app.model.CommonEntity;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * DosenPembimbing
 */
@Entity
@Table(name = DosenPembimbing.NAMA_TABLE)
public class DosenPembimbing extends CommonEntity {
  private static final long serialVersionUID = -3186229068218752652L;

  public static final String NAMA_TABLE = "t_dosen_pembimbing";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private Long id;

  @ManyToOne
  @JoinColumn(name = "kode_dosen", nullable = false, referencedColumnName = "id")
  private Dosen kodeDosen;

  @ManyToOne
  @JoinColumn(name = "kode_mahasiswa", nullable = false, referencedColumnName = "id")
  private Mahasiswa kodeMahasiswa;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "GMT+07:00")
  @Column(name = "tanggal_mulai")
  private Date tanggalMulai;

  public DosenPembimbing id(Long id) {
    this.id = id;
    return this;
  }

  public DosenPembimbing kodeDosen(Dosen kodeDosen) {
    this.kodeDosen = kodeDosen;
    return this;
  }

  public DosenPembimbing kodeMahasiswa(Mahasiswa kodeMahasiswa) {
    this.kodeMahasiswa = kodeMahasiswa;
    return this;
  }

  public DosenPembimbing tanggalMulai(Date tanggalMulai) {
    this.tanggalMulai = tanggalMulai;
    return this;
  }

  public DosenPembimbing(Long id, Dosen kodeDosen, Mahasiswa kodeMahasiswa, Date tanggalMulai) {
    this.id = id;
    this.kodeDosen = kodeDosen;
    this.kodeMahasiswa = kodeMahasiswa;
    this.tanggalMulai = tanggalMulai;
  }

  public DosenPembimbing() {
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Dosen getKodeDosen() {
    return this.kodeDosen;
  }

  public void setKodeDosen(Dosen kodeDosen) {
    this.kodeDosen = kodeDosen;
  }

  public Mahasiswa getKodeMahasiswa() {
    return this.kodeMahasiswa;
  }

  public void setKodeMahasiswa(Mahasiswa kodeMahasiswa) {
    this.kodeMahasiswa = kodeMahasiswa;
  }

  public Date getTanggalMulai() {
    return this.tanggalMulai;
  }

  public void setTanggalMulai(Date tanggalMulai) {
    this.tanggalMulai = tanggalMulai;
  }

}