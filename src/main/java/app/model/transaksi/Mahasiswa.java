package app.model.transaksi;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import app.model.CommonEntity;
import app.model.master.Agama;
import app.model.master.Jurusan;

@Entity
@Table(name = Mahasiswa.TABLE_NAME)
public class Mahasiswa extends CommonEntity {

   private static final long serialVersionUID = -2315121322790538532L;

   public static final String TABLE_NAME = "t_mahasiswa";

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Long id;

   @Column(name = "kode_mahasiswa", nullable = false, length = 5)
   @Size(max = 5)
   private String kodeMahasiswa;

   @Column(name = "nama_mahasiswa", nullable = false, length = 100)
   private String namaMahasiswa;

   @Column(name = "alamat_mahasiswa", nullable = false, length = 100)
   private String alamatMahasiswa;

   @ManyToOne
   @JoinColumn(name = "kode_agama", nullable = false, referencedColumnName = "id")
   private Agama kodeAgama;

   @ManyToOne
   @JoinColumn(name = "kode_jurusan", nullable = false, referencedColumnName = "id")
   private Jurusan kodeJurusan;

   @Column(name = "kode_pos", length = 5)
   @Size(max = 5)
   private String kodePos;

   @Column(name = "no_hp", length = 15)
   @Size(max = 15)
   private String nomorHanphone;

   public Mahasiswa() {
   }

   public Mahasiswa(Long id, String kodeMahasiswa, String namaMahasiswa, String alamatMahasiswa, Agama kodeAgama,
         Jurusan kodeJurusan, String kodePos, String nomorHanphone) {
      this.id = id;
      this.kodeMahasiswa = kodeMahasiswa;
      this.namaMahasiswa = namaMahasiswa;
      this.alamatMahasiswa = alamatMahasiswa;
      this.kodeAgama = kodeAgama;
      this.kodeJurusan = kodeJurusan;
      this.kodePos = kodePos;
      this.nomorHanphone = nomorHanphone;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getKodeMahasiswa() {
      return kodeMahasiswa;
   }

   public void setKodeMahasiswa(String kodeMahasiswa) {
      this.kodeMahasiswa = kodeMahasiswa;
   }

   public String getNamaMahasiswa() {
      return namaMahasiswa;
   }

   public void setNamaMahasiswa(String namaMahasiswa) {
      this.namaMahasiswa = namaMahasiswa;
   }

   public String getAlamatMahasiswa() {
      return alamatMahasiswa;
   }

   public void setAlamatMahasiswa(String alamatMahasiswa) {
      this.alamatMahasiswa = alamatMahasiswa;
   }

   public Agama getKodeAgama() {
      return kodeAgama;
   }

   public void setKodeAgama(Agama kodeAgama) {
      this.kodeAgama = kodeAgama;
   }

   public Jurusan getKodeJurusan() {
      return kodeJurusan;
   }

   public void setKodeJurusan(Jurusan kodeJurusan) {
      this.kodeJurusan = kodeJurusan;
   }

   public String getKodePos() {
      return kodePos;
   }

   public void setKodePos(String kodePos) {
      this.kodePos = kodePos;
   }

   public String getNomorHanphone() {
      return nomorHanphone;
   }

   public void setNomorHanphone(String nomorHanphone) {
      this.nomorHanphone = nomorHanphone;
   }

}
