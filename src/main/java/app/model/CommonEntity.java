package app.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * CommonEntity
 */
@MappedSuperclass
public class CommonEntity implements Serializable {

  private static final long serialVersionUID = 4480188634268342216L;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+07:00")
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "created_on", nullable = false)
  private Date createdOn;

  @Column(name = "created_by", nullable = false)
  private Long createdBy;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+07:00")
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "modified_on")
  private Date modifedOn;

  @Column(name = "modified_by")
  private Long modifedBy;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+07:00")
  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "deleted_on")
  private Date deletedOn;

  @Column(name = "deleted_by")
  private Long deletedBy;

  @Column(name = "isdelete", nullable = false)
  private Boolean isDelete = false;

  public CommonEntity() {
  }

  public CommonEntity(Date createdOn, Long createdBy, Date modifedOn, Long modifedBy, Date deletedOn, Long deletedBy,
      Boolean isDelete) {
    this.createdOn = createdOn;
    this.createdBy = createdBy;
    this.modifedOn = modifedOn;
    this.modifedBy = modifedBy;
    this.deletedOn = deletedOn;
    this.deletedBy = deletedBy;
    this.isDelete = isDelete;
  }

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Long getCreatedBy() {
    return createdBy;
  }

  public void setCreatedBy(Long createdBy) {
    this.createdBy = createdBy;
  }

  public Date getModifedOn() {
    return modifedOn;
  }

  public void setModifedOn(Date modifiedOn) {
    this.modifedOn = modifiedOn;
  }

  public Long getModifedBy() {
    return modifedBy;
  }

  public void setModifedBy(Long modifiedBy) {
    this.modifedBy = modifiedBy;
  }

  public Date getDeletedOn() {
    return deletedOn;
  }

  public void setDeletedOn(Date deleteddOn) {
    this.deletedOn = deleteddOn;
  }

  public Long getDeletedBy() {
    return deletedBy;
  }

  public void setDeletedBy(Long deletedBy) {
    this.deletedBy = deletedBy;
  }

  public Boolean getIsDelete() {
    return isDelete;
  }

  public void setIsDelete(Boolean isDelete) {
    this.isDelete = isDelete;
  }

}