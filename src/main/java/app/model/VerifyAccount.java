package app.model;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * VerifyAccount
 */

@Entity
@Table(name = VerifyAccount.NAMA_TABLE)
public class VerifyAccount {

  public static final String NAMA_TABLE = "verify_account";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Lob
  @Column(name = "token", columnDefinition = "text", unique = true)
  @org.hibernate.annotations.Type(type = "org.hibernate.type.TextType")
  private String token;

  @Column
  private LocalDateTime expiredDataToken;

  @Column
  private LocalDateTime createdDate;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "account_id")
  private Account account;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public LocalDateTime getExpiredDataToken() {
    return expiredDataToken;
  }

  public void setExpiredDataToken(LocalDateTime expiredDataToken) {
    this.expiredDataToken = expiredDataToken;
  }

  public LocalDateTime getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(LocalDateTime createdDate) {
    this.createdDate = createdDate;
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public void setExpiredDataToken(int menit) {
    Calendar waktuSaatIni = Calendar.getInstance();
    waktuSaatIni.add(Calendar.MINUTE, menit);
    this.expiredDataToken = waktuSaatIni.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
  }

  public boolean isExpired() {
    return LocalDateTime.now().isAfter(expiredDataToken);
  }
}