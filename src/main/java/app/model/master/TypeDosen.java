package app.model.master;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = TypeDosen.TABLE_NAME)
public class TypeDosen implements Serializable {
   private static final long serialVersionUID = -5700679945285035073L;

   public static final String TABLE_NAME = "m_type_dosen";

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private Long id;

   @Column(name = "kode_typedosen", nullable = false, length = 5)
   @Size(max = 5)
   private String kodeTypeDosen;

   @Column(name = "deskripsi", length = 20)
   private String deskripsi;

   public TypeDosen() {
   }

   public TypeDosen(Long id, @Size(max = 5) String kodeTypeDosen, String deskripsi) {
      this.id = id;
      this.kodeTypeDosen = kodeTypeDosen;
      this.deskripsi = deskripsi;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getKodeTypeDosen() {
      return kodeTypeDosen;
   }

   public void setKodeTypeDosen(String kodeTypeDosen) {
      this.kodeTypeDosen = kodeTypeDosen;
   }

   public String getDeskripsi() {
      return deskripsi;
   }

   public void setDeskripsi(String deskripsi) {
      this.deskripsi = deskripsi;
   }

}
