package app.model.master;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = Jurusan.TABLE_NAME)
public class Jurusan implements Serializable {
   private static final long serialVersionUID = 2983943736775299525L;

   public static final String TABLE_NAME = "m_jurusan";

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private Long id;

   @Column(name = "kode_jurusan", nullable = false, length = 5)
   @Size(max = 5)
   private String kodeJurusan;

   @Column(name = "nama_jurusan")
   private String namaJurusan;

   @Column(name = "status_jurusan")
   private String statusJurusan;

   public Jurusan() {
   }

   public Jurusan(Long id, String kodeJurusan, String namaJurusan, String statusJurusan) {
      this.id = id;
      this.kodeJurusan = kodeJurusan;
      this.namaJurusan = namaJurusan;
      this.statusJurusan = statusJurusan;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getKodeJurusan() {
      return kodeJurusan;
   }

   public void setKodeJurusan(String kodeJurusan) {
      this.kodeJurusan = kodeJurusan;
   }

   public String getNamaJurusan() {
      return namaJurusan;
   }

   public void setNamaJurusan(String namaJurusan) {
      this.namaJurusan = namaJurusan;
   }

   public String getStatusJurusan() {
      return statusJurusan;
   }

   public void setStatusJurusan(String statusJurusan) {
      this.statusJurusan = statusJurusan;
   }

}
