package app.model.master;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Table(name = Agama.TABLE_NAME)
public class Agama implements Serializable {
   private static final long serialVersionUID = -8724230319846084590L;

   public static final String TABLE_NAME = "m_agama";

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Long id;

   @Column(name = "kode_agama", nullable = false, length = 5)
   @Size(max = 5)
   private String kodeAgama;

   @Column(name = "deskripsi")
   private String deskripsi;

   public Agama() {
   }

   public Agama(Long id, String kodeAgama, String deskripsi) {
      this.id = id;
      this.kodeAgama = kodeAgama;
      this.deskripsi = deskripsi;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getKodeAgama() {
      return kodeAgama;
   }

   public void setKodeAgama(String kodeAgama) {
      this.kodeAgama = kodeAgama;
   }

   public String getDeskripsi() {
      return deskripsi;
   }

   public void setDeskripsi(String deskripsi) {
      this.deskripsi = deskripsi;
   }

}
