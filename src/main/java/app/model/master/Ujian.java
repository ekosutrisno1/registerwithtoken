package app.model.master;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = Ujian.TABLE_NAME)
public class Ujian implements Serializable {
   private static final long serialVersionUID = 7044671251061296703L;

   public static final String TABLE_NAME = "m_ujian";

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id")
   private Long id;

   @Column(name = "kode_ujian", nullable = false, length = 5)
   @Size(max = 5)
   private String kodeUjian;

   @Column(name = "nama_ujian", length = 50)
   private String namaUjian;

   @Column(name = "status_ujian", length = 50)
   private String statusUjian;

   public Ujian() {
   }

   public Ujian(Long id, @Size(max = 5) String kodeUjian, String namaUjian, String statusUjian) {
      this.id = id;
      this.kodeUjian = kodeUjian;
      this.namaUjian = namaUjian;
      this.statusUjian = statusUjian;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getKodeUjian() {
      return kodeUjian;
   }

   public void setKodeUjian(String kodeUjian) {
      this.kodeUjian = kodeUjian;
   }

   public String getNamaUjian() {
      return namaUjian;
   }

   public void setNamaUjian(String namaUjian) {
      this.namaUjian = namaUjian;
   }

   public String getStatusUjian() {
      return statusUjian;
   }

   public void setStatusUjian(String statusUjian) {
      this.statusUjian = statusUjian;
   }

}
