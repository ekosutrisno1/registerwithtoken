package app.model.view;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

/**
 * ViewNilai
 */
@Entity
@Immutable
@Table(name = ViewNilai.TABLE_NAME)
public class ViewNilai implements Serializable {

  private static final long serialVersionUID = 8912792798128352070L;

  public static final String TABLE_NAME = "get_nilai";

  @Id
  @Column(name = "id")
  private Long id;

  @Column(name = "isdelete")
  private boolean isDelete;

  @Column(name = "kode_mahasiswa")
  private String kodeMahasiswa;

  @Column(name = "kode_ujian")
  private String kodeUjian;

  @Column(name = "nilai")
  private String nilai;

  public Long getId() {
    return id;
  }

  public boolean isDelete() {
    return isDelete;
  }

  public String getKodeMahasiswa() {
    return kodeMahasiswa;
  }

  public String getKodeUjian() {
    return kodeUjian;
  }

  public String getNilai() {
    return nilai;
  }

}