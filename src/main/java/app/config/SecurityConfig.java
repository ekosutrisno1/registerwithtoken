package app.config;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import app.principal.CustomUserDetailService;

/**
 * SecurityConfig
 */

@Configuration
@EnableAutoConfiguration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Autowired
  private CustomUserDetailService userDetailsService;

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()

        .antMatchers("/home", "/sign-up").permitAll()

        .antMatchers("/sign-up**", "/forgot-password**", "/reset-password**").permitAll()

        .antMatchers("/app/profile/**").authenticated()

        .antMatchers("/app/dashboard/**").hasAnyRole("ADMIN", "USER")

        .antMatchers("/app/management/**").hasAnyRole("ADMIN", "MANAGER")

        .antMatchers("/rest/**").hasRole("ADMIN")

        .and().formLogin()

        .loginPage("/login").permitAll()

        .and().rememberMe().tokenValiditySeconds((int) TimeUnit.DAYS.toSeconds(2)).key("ekosutrisno")

        .and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login?logout")

        .clearAuthentication(true).invalidateHttpSession(true)

        .deleteCookies("JSESSIONID", "remember-me");

  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

}