package app.service.account;

import java.util.Optional;

import javax.mail.MessagingException;

import app.dto.AccountCreateDto;
import app.dto.ResendCodeDto;
import app.dto.VerifyCodeDto;
import app.model.Account;

/**
 * AccountService
 */
public interface AccountService {

  public Account createMember(AccountCreateDto accountCreateDto) throws Exception;

  public Account createAdmin(AccountCreateDto accountCreateDto);

  Optional<Account> findByUsernameOrEmail(String username, String email);

  Optional<Account> findByEmail(String email);

  Optional<Account> findByUsername(String username);

  Optional<Account> findById(Long id);

  public void verifyCode(VerifyCodeDto verifyCodeDto);

  public void resendCode(ResendCodeDto resendCodeDto) throws MessagingException;

  public void updatePassword(String password, Long userId);

  public Account findByEmailOne(String email);
}