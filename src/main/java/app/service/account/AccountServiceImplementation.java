package app.service.account;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import app.dao.account.AccountDao;
import app.dao.verify_account.VerifyAccountDao;
import app.dto.AccountCreateDto;
import app.dto.ResendCodeDto;
import app.dto.VerifyCodeDto;
import app.mail.Mail;
import app.mail.MailService;
import app.model.Account;
import app.model.Role;
import app.model.VerifyAccount;
import app.service.role.RoleService;
import app.util.RandomUtil;

/**
 * AccountServiceImplementation
 */

@Service
public class AccountServiceImplementation implements AccountService {

  @Autowired
  private AccountDao accountDao;

  @Autowired
  private VerifyAccountDao verifuAccounDao;

  @Autowired
  private RoleService roleService;

  @Autowired
  private MailService mailService;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  public Account createMember(AccountCreateDto accountCreateDto) throws Exception {

    String email = accountCreateDto.getEmail();
    String username = accountCreateDto.getUsername();
    String password = accountCreateDto.getPassword();

    Account account = new Account();

    account.setEmail(email);
    account.setUsername(username);
    account.setPassword(passwordEncoder.encode(password));
    account.setActive(false);

    if (roleService.findById(2L).isPresent()) {
      Role role = roleService.findById(2L).get();
      account.addRole(role);
    }

    String token = RandomUtil.generateRandomStringNumber(6).toUpperCase();

    VerifyAccount verifyAccount = new VerifyAccount();
    verifyAccount.setAccount(account);
    verifyAccount.setCreatedDate(LocalDateTime.now());
    verifyAccount.setExpiredDataToken(5);
    verifyAccount.setToken(token);
    verifuAccounDao.create(verifyAccount);

    Map<String, Object> maps = new HashMap<>();
    maps.put("account", account);
    maps.put("token", token);

    Mail mail = new Mail();
    mail.setFrom("xsisappv2.0@gmail.com");
    mail.setSubject("Registration");
    mail.setTo(account.getEmail());
    mail.setModel(maps);
    mailService.sendEmail(mail);

    return accountDao.create(account);

  }

  @Override
  public Account createAdmin(AccountCreateDto accountCreateDto) {
    String email = accountCreateDto.getEmail();
    String username = accountCreateDto.getUsername();
    String password = accountCreateDto.getPassword();

    Account account = new Account();

    account.setEmail(email);
    account.setUsername(username);
    account.setPassword(passwordEncoder.encode(password));

    if (roleService.findById(2L).isPresent()) {
      Role role = roleService.findById(2L).get();
      account.addRole(role);
    }

    return accountDao.create(account);
  }

  @Override
  public Optional<Account> findByUsernameOrEmail(String username, String email) {
    return accountDao.findByUsernameOrEmail(username, email);
  }

  @Override
  public Optional<Account> findByEmail(String email) {
    return accountDao.findByEmail(email);
  }

  @Override
  public Optional<Account> findByUsername(String username) {
    return accountDao.findByUsername(username);
  }

  @Override
  public Optional<Account> findById(Long id) {
    return accountDao.findById(id);
  }

  @Override
  public void verifyCode(VerifyCodeDto verifyCodeDto) {

    String data = verifyCodeDto.getToken();
    String token = data.toUpperCase();
    System.out.println(token);

    VerifyAccount verifyAccount = verifuAccounDao.findByToken(token).get();
    Account account = verifyAccount.getAccount();
    account.setActive(true);
    accountDao.update(account);
  }

  @Override
  public void updatePassword(String password, Long userId) {
    accountDao.updatePassword(password, userId);
  }

  @Override
  public Account findByEmailOne(String email) {
    return accountDao.findByEmailOne(email);
  }

  @Override
  public void resendCode(ResendCodeDto resendCodeDto) throws MessagingException {
    String emailUser = resendCodeDto.getEmail();
    String token = RandomUtil.generateRandomStringNumber(6).toUpperCase();

    Account user = accountDao.findByEmail(emailUser).get();
    VerifyAccount data = verifuAccounDao.findByAccount(user.getId());

    data.setAccount(user);
    data.setCreatedDate(LocalDateTime.now());
    data.setExpiredDataToken(5);
    data.setToken(token);
    verifuAccounDao.update(data);

    Map<String, Object> maps = new HashMap<>();
    maps.put("token", token);
    maps.put("account", user);

    Mail mail = new Mail();
    mail.setFrom("xsisappv2.0@gmail.com");
    mail.setSubject("Resend Kode Verifikasi");
    mail.setTo(resendCodeDto.getEmail());
    mail.setModel(maps);
    mailService.sendEmail(mail);
  }

}