package app.service.data_service;

import java.util.List;
import java.util.Optional;

import app.model.transaksi.Dosen;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * DosenService
 */
public interface DosenService {

  List<Dosen> getAll();

  Page<Dosen> getPaging(Pageable pageable);

  Optional<Dosen> getById(Long id);

  Dosen save(Dosen dosen);

  Dosen update(Dosen dosen);

  Dosen delete(Long id);

}