package app.service.data_service;

import java.util.List;
import java.util.Optional;

import app.model.transaksi.Mahasiswa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * MahasiswaService
 */
public interface MahasiswaService {

  List<Mahasiswa> getAllMahasiswa();

  Page<Mahasiswa> getUsingPaging(Pageable pageable);

  Optional<Mahasiswa> getMahasiswaById(Long id);

  Mahasiswa saveMahasiswa(Mahasiswa mahasiswa);

  Mahasiswa updateMahasiswa(Mahasiswa mahasiswa);

  Mahasiswa deleteMahasiswa(Long id);

}