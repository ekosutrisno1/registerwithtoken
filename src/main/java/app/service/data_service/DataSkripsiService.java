package app.service.data_service;

import java.util.List;
import java.util.Optional;

import app.model.transaksi.DataSkripsi;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * DataSkripsiService
 */
public interface DataSkripsiService {

  Page<DataSkripsi> getAllPagingPage(Pageable pageable);

  List<DataSkripsi> getAll();

  Optional<DataSkripsi> getById(Long id);

  DataSkripsi save(DataSkripsi dataSkripsi);

  DataSkripsi update(DataSkripsi dataSkripsi);

  DataSkripsi delete(Long id);
}