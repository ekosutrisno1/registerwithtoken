package app.service.data_service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.dao.transaksi.MahasiswaDao;
import app.model.transaksi.Mahasiswa;
import app.service.data_service.MahasiswaService;

/**
 * MahasiswaServiceImpl
 */
@Service
@Transactional
public class MahasiswaServiceImpl implements MahasiswaService {
  @Autowired
  private MahasiswaDao mhsRepo;

  @Override
  public List<Mahasiswa> getAllMahasiswa() {
    List<Mahasiswa> mahasiswa = new ArrayList<>();
    for (Mahasiswa mhs : mhsRepo.findAll()) {
      if (!mhs.getIsDelete())
        mahasiswa.add(mhs);
    }
    return mahasiswa;
  }

  @Override
  public Optional<Mahasiswa> getMahasiswaById(Long id) {
    return mhsRepo.findById(id);
  }

  @Override
  public Mahasiswa saveMahasiswa(Mahasiswa mahasiswa) {
    mahasiswa.setCreatedOn(new Date());
    mahasiswa.setCreatedBy(1L);
    return mhsRepo.create(mahasiswa);
  }

  @Override
  public Mahasiswa updateMahasiswa(Mahasiswa mahasiswa) {
    Mahasiswa dataMahasiswa = mhsRepo.findById(mahasiswa.getId()).get();
    dataMahasiswa.setId(mahasiswa.getId());
    dataMahasiswa.setNamaMahasiswa(mahasiswa.getNamaMahasiswa());
    dataMahasiswa.setKodeMahasiswa(mahasiswa.getKodeMahasiswa());
    dataMahasiswa.setAlamatMahasiswa(mahasiswa.getAlamatMahasiswa());
    dataMahasiswa.setKodeJurusan(mahasiswa.getKodeJurusan());
    dataMahasiswa.setKodeAgama(mahasiswa.getKodeAgama());
    dataMahasiswa.setKodePos(mahasiswa.getKodePos());
    dataMahasiswa.setNomorHanphone(mahasiswa.getNomorHanphone());

    dataMahasiswa.setModifedBy(2L);
    dataMahasiswa.setModifedOn(new Date());
    return mhsRepo.update(dataMahasiswa);

  }

  @Override
  public Mahasiswa deleteMahasiswa(Long id) {
    Mahasiswa mahasiswa = mhsRepo.findById(id).get();

    mahasiswa.setIsDelete(true);
    mahasiswa.setDeletedBy(3L);
    mahasiswa.setDeletedOn(new Date());

    return mhsRepo.update(mahasiswa);
  }

  @Override
  public Page<Mahasiswa> getUsingPaging(Pageable pageable) {
    return mhsRepo.getPaging(pageable);
  }

}