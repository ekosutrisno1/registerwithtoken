package app.service.data_service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.dao.transaksi.DataSkripsiDao;
import app.model.transaksi.DataSkripsi;
import app.service.data_service.DataSkripsiService;

/**
 * DataSkripsiImpl
 */
@Service
public class DataSkripsiImpl implements DataSkripsiService {

  @Autowired
  private DataSkripsiDao dataSkripsiRepo;

  @Override
  public Page<DataSkripsi> getAllPagingPage(Pageable pageable) {
    return dataSkripsiRepo.getAllPaging(pageable);
  }

  @Override
  public List<DataSkripsi> getAll() {
    List<DataSkripsi> dataSkripsi = new ArrayList<>();
    for (DataSkripsi data : dataSkripsiRepo.findAll()) {
      if (!data.getIsDelete()) {
        dataSkripsi.add(data);
      }
    }
    return dataSkripsi;
  }

  @Override
  public Optional<DataSkripsi> getById(Long id) {
    return dataSkripsiRepo.findById(id);
  }

  @Override
  public DataSkripsi save(DataSkripsi dataSkripsi) {
    dataSkripsi.setCreatedOn(new Date());
    dataSkripsi.setCreatedBy(3L);
    dataSkripsi.setTanggalMulai(new Date());
    return dataSkripsiRepo.create(dataSkripsi);
  }

  @Override
  public DataSkripsi update(DataSkripsi dataSkripsi) {
    DataSkripsi skripsi = dataSkripsiRepo.findById(dataSkripsi.getId()).get();

    skripsi.setId(dataSkripsi.getId());
    skripsi.setKodeMahasiswa(dataSkripsi.getKodeMahasiswa());
    skripsi.setJudulSkripsi(dataSkripsi.getJudulSkripsi());

    skripsi.setModifedBy(2L);
    skripsi.setModifedOn(new Date());
    return dataSkripsiRepo.update(skripsi);
  }

  @Override
  public DataSkripsi delete(Long id) {
    DataSkripsi skripsi = dataSkripsiRepo.findById(id).get();

    skripsi.setIsDelete(true);
    skripsi.setDeletedBy(1L);
    skripsi.setDeletedOn(new Date());

    return dataSkripsiRepo.update(skripsi);
  }

}