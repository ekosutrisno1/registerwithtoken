package app.service.data_service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import app.dao.transaksi.NilaiDao;
import app.dao.view.ViewNilaiDao;
import app.model.transaksi.Nilai;
import app.model.view.ViewNilai;
import app.service.data_service.NilaiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * NilaiServiceImpl
 */
@Service
@Transactional
public class NilaiServiceImpl implements NilaiService {

  @Autowired
  private NilaiDao nilaiRepo;

  @Autowired
  private ViewNilaiDao viewRepo;

  @Override
  public List<Nilai> getAll() {
    List<Nilai> dataNilai = new ArrayList<>();
    for (Nilai nilai : nilaiRepo.findAll()) {
      if (!nilai.getIsDelete()) {
        dataNilai.add(nilai);
      }
    }
    return dataNilai;
  }

  @Override
  public List<ViewNilai> getAllView() {
    List<ViewNilai> dataNilai = new ArrayList<>();
    for (ViewNilai nilai : viewRepo.getAllNilaiView()) {
      dataNilai.add(nilai);
    }
    return dataNilai;
  }

  @Override
  public Page<Nilai> getAllPaging(Pageable pageable) {
    return nilaiRepo.pagination(pageable);
  }

  @Override
  public Optional<Nilai> getById(Long id) {
    return nilaiRepo.findById(id);
  }

  @Override
  public Nilai save(Nilai nilai) {
    nilai.setCreatedBy(1L);
    nilai.setCreatedOn(new Date());
    return nilaiRepo.create(nilai);

  }

  @Override
  public Nilai update(Nilai nilai) {
    Nilai dataNilai = nilaiRepo.findById(nilai.getId()).get();

    dataNilai.setId(nilai.getId());
    dataNilai.setKodeMahasiswa(nilai.getKodeMahasiswa());
    dataNilai.setKodeUjian(nilai.getKodeUjian());
    dataNilai.setNilaiMahasiswa(nilai.getNilaiMahasiswa());

    dataNilai.setModifedBy(02L);
    dataNilai.setCreatedOn(new Date());
    return nilaiRepo.update(dataNilai);
  }

  @Override
  public Nilai delete(Long id) {
    Nilai dataNilai = nilaiRepo.findById(id).get();

    dataNilai.setDeletedBy(04L);
    dataNilai.setDeletedOn(new Date());
    dataNilai.setIsDelete(true);

    return nilaiRepo.update(dataNilai);
  }

}