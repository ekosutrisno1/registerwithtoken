package app.service.data_service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.dao.master.AgamaDao;
import app.model.master.Agama;
import app.service.data_service.AgamaService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AgamaServiceImpl implements AgamaService {
   @Autowired
   private AgamaDao agamaRepository;

   @Override
   public List<Agama> getAll() {
      return new ArrayList<>(agamaRepository.findAll());
   }

   @Override
   public Optional<Agama> getById(Long id) {
      return agamaRepository.findById(id);
   }
}
