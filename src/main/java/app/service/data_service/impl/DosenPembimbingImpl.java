package app.service.data_service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import app.dao.transaksi.DosenPembimbingDao;
import app.model.transaksi.DosenPembimbing;
import app.service.data_service.DosenPembimbingService;

/**
 * DosenPembimbingImpl
 */
@Service
public class DosenPembimbingImpl implements DosenPembimbingService {

  @Autowired
  private DosenPembimbingDao dosenPembimbingRepository;

  @Override
  public Page<DosenPembimbing> getAllPagingPage(Pageable pageable) {
    return dosenPembimbingRepository.getAllPaging(pageable);
  }

  @Override
  public List<DosenPembimbing> getAll() {
    List<DosenPembimbing> dataDosen = new ArrayList<>();
    for (DosenPembimbing dosPem : dosenPembimbingRepository.findAll()) {
      if (!dosPem.getIsDelete()) {
        dataDosen.add(dosPem);
      }
    }
    return dataDosen;
  }

  @Override
  public Optional<DosenPembimbing> getById(Long id) {
    return dosenPembimbingRepository.findById(id);
  }

  @Override
  public DosenPembimbing save(DosenPembimbing dosenPembimbing) {
    dosenPembimbing.setCreatedBy(1L);
    dosenPembimbing.setCreatedOn(new Date());
    dosenPembimbing.setTanggalMulai(new Date());
    return dosenPembimbingRepository.create(dosenPembimbing);
  }

  @Override
  public DosenPembimbing update(DosenPembimbing dosenPembimbing) {
    DosenPembimbing dataDosen = dosenPembimbingRepository.findById(dosenPembimbing.getId()).get();

    dataDosen.setId(dosenPembimbing.getId());
    dataDosen.setKodeDosen(dosenPembimbing.getKodeDosen());
    dataDosen.setKodeMahasiswa(dosenPembimbing.getKodeMahasiswa());
    dataDosen.setTanggalMulai(dosenPembimbing.getTanggalMulai());

    dataDosen.setModifedBy(2L);
    dataDosen.setModifedOn(new Date());

    return dosenPembimbingRepository.update(dataDosen);
  }

  @Override
  public DosenPembimbing delete(Long id) {
    DosenPembimbing dosen = dosenPembimbingRepository.findById(id).get();

    dosen.setDeletedBy(1L);
    dosen.setDeletedOn(new Date());
    dosen.setIsDelete(true);

    return dosenPembimbingRepository.update(dosen);
  }

}