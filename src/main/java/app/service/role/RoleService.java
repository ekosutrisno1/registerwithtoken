package app.service.role;

import java.util.Optional;

import app.model.Role;

/**
 * RoleService
 */
public interface RoleService {

  Optional<Role> findById(Long id);

  Role createRole(Role role);

}