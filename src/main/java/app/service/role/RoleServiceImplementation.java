package app.service.role;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.dao.role.RoleDao;
import app.model.Role;

/**
 * RoleServiceImplementation
 */

@Service
public class RoleServiceImplementation implements RoleService {

  @Autowired
  private RoleDao roleDao;

  @Override
  public Optional<Role> findById(Long id) {
    return roleDao.findById(id);
  }

  @Override
  public Role createRole(Role role) {
    return roleDao.create(role);
  }

}