package app.service.accout_verify;

import java.util.Optional;

import app.model.VerifyAccount;

/**
 * AccountVerifyService
 */
public interface AccountVerifyService {
  VerifyAccount create(VerifyAccount verifyAccount);

  VerifyAccount findByAccount(Long id);

  Optional<VerifyAccount> findByToken(String token);

  Optional<VerifyAccount> findById(Long id);

  public void updateToken(String token, Long userId);

}