package app.service.accout_verify;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.dao.verify_account.VerifyAccountDao;
import app.model.VerifyAccount;

/**
 * AccountVerifyServiceImplementation
 */
@Service
public class AccountVerifyServiceImplementation implements AccountVerifyService {

  @Autowired
  private VerifyAccountDao verifyAccountDao;

  @Override
  public VerifyAccount create(VerifyAccount verifyAccount) {
    return verifyAccountDao.create(verifyAccount);
  }

  @Override
  public Optional<VerifyAccount> findByToken(String token) {
    return verifyAccountDao.findByToken(token);
  }

  @Override
  public Optional<VerifyAccount> findById(Long id) {
    return verifyAccountDao.findById(id);
  }

  @Override
  public void updateToken(String token, Long userId) {
    verifyAccountDao.updateToken(token, userId);
  }

  @Override
  public VerifyAccount findByAccount(Long id) {
    return verifyAccountDao.findByAccount(id);
  }

}