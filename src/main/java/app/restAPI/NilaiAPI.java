package app.restAPI;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.model.transaksi.Nilai;
import app.model.view.ViewNilai;
import app.service.data_service.NilaiService;

/**
 * NilaiAPI
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "rest/nilai", produces = "application/json")
public class NilaiAPI {

  @Autowired
  private NilaiService nilaiService;

  @Cacheable(value = "nilai-cache")
  @GetMapping
  public List<Nilai> getAllNilai() {
    return nilaiService.getAll();
  }

  @Cacheable(value = "nilai-cache")
  @GetMapping("/view")
  public List<ViewNilai> getAllNilaiView() {
    return nilaiService.getAllView();
  }

  @Cacheable(value = "nilai-cache")
  @GetMapping("/page")
  public Page<Nilai> getPageNilai(@PageableDefault(size = 10, sort = "id") Pageable pageable) {
    return nilaiService.getAllPaging(pageable);
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getNilaiById(@PathVariable("id") Long id) {
    Optional<Nilai> optNilai = nilaiService.getById(id);
    if (optNilai.isPresent()) {
      return new ResponseEntity<>(optNilai.get(), HttpStatus.OK);
    }
    return new ResponseEntity<>("Data-tidak-ada!", HttpStatus.NOT_FOUND);
  }

  @CachePut(value = "nilai-cache", key = "id")
  @PostMapping
  public Nilai tambahNilai(@Valid @RequestBody Nilai nilai) {
    return nilaiService.save(nilai);
  }

  @PutMapping
  public Nilai updateNilai(@Valid @RequestBody Nilai nilai) {
    return nilaiService.update(nilai);
  }

  @DeleteMapping("/{id}")
  public Nilai deleteNilai(@PathVariable("id") Long id) {
    return nilaiService.delete(id);
  }

}