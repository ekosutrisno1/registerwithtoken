package app.restAPI;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.model.transaksi.Dosen;
import app.service.data_service.DosenService;

/**
 * DosenAPI
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "rest/dosen", produces = "application/json")
public class DosenAPI {

  @Autowired
  private DosenService dosenService;

//  @Cacheable(value = "dosen-cache")
  @GetMapping
  public List<Dosen> getAllDosen() {
    return dosenService.getAll();
  }

//  @Cacheable(value = "dosen-cache")
  @GetMapping("/page")
  public Page<Dosen> getPagingDosen(@PageableDefault(size = 10, sort = "id") Pageable pageable) {
    return dosenService.getPaging(pageable);
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getDosenById(@PathVariable("id") Long id) {
    Optional<Dosen> dosenId = dosenService.getById(id);
    if (dosenId.isPresent()) {
      return new ResponseEntity<>(dosenId.get(), HttpStatus.OK);
    }
    return new ResponseEntity<>("Data-tidak-ada!", HttpStatus.NOT_FOUND);
  }

  @PostMapping
  public Dosen saveDosen(@RequestBody Dosen dosen) {
    return dosenService.save(dosen);
  }

  @PutMapping
  public Dosen updatDosen(@Valid @RequestBody Dosen dosen) {
    return dosenService.update(dosen);
  }

  @DeleteMapping("/{id}")
  public Dosen deleDosen(@PathVariable("id") Long id) {
    return dosenService.delete(id);
  }
}