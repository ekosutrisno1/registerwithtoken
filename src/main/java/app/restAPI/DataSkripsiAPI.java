package app.restAPI;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.model.transaksi.DataSkripsi;
import app.service.data_service.DataSkripsiService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * DataSkripsiAPI
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "rest/data-skripsi", produces = "application/json")
public class DataSkripsiAPI {
  @Autowired
  private DataSkripsiService dataSkripsiService;

//  @Cacheable(value = "data-skripsi-cache")
  @GetMapping
  public List<DataSkripsi> getAllDataSkripsi() {
    return dataSkripsiService.getAll();
  }

//  @Cacheable(value = "data-skripsi-cache")
  @GetMapping("/page")
  public Page<DataSkripsi> getAllPaging(Pageable pageable) {
    System.out.println("Cache-mulai");
    return dataSkripsiService.getAllPagingPage(pageable);
  }

//  @Cacheable(value = "data-skripsi-cache")
  @GetMapping("/{id}")
  public Optional<DataSkripsi> getDataById(@PathVariable("id") Long id) {
    return dataSkripsiService.getById(id);
  }

  @PostMapping
  public DataSkripsi tambahDataSkripsi(@RequestBody DataSkripsi dataSkripsi) {
    return dataSkripsiService.save(dataSkripsi);
  }

  @PutMapping
  public DataSkripsi editDataSkripsi(@RequestBody DataSkripsi dataSkripsi) {
    return dataSkripsiService.update(dataSkripsi);
  }

  @DeleteMapping("/{id}")
  public DataSkripsi deletDataSkripsi(@PathVariable("id") Long id) {
    return dataSkripsiService.delete(id);
  }

}