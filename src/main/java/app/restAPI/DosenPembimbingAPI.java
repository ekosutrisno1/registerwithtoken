package app.restAPI;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.model.transaksi.DosenPembimbing;
import app.service.data_service.DosenPembimbingService;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * DosenPembimbingAPI
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "rest/dosen-pembimbing", produces = "application/json")
public class DosenPembimbingAPI {

  @Autowired
  private DosenPembimbingService dosenPembimbingService;

  @GetMapping
  public List<DosenPembimbing> getAllData() {
    return dosenPembimbingService.getAll();
  }

  @GetMapping("/page")
  public Page<DosenPembimbing> getAllDataPaging(@PageableDefault(size = 10, sort = "id") Pageable pageable) {
    return dosenPembimbingService.getAllPagingPage(pageable);
  }

  @GetMapping("/{id}")
  public ResponseEntity<?> getDataById(@PathVariable("id") Long id) {
    Optional<DosenPembimbing> optDosPem = dosenPembimbingService.getById(id);
    if (optDosPem.isPresent()) {
      return new ResponseEntity<>(optDosPem.get(), HttpStatus.OK);
    }
    return new ResponseEntity<>("Data-Tidak-Ada.", HttpStatus.NOT_FOUND);
  }

  @PostMapping
  public DosenPembimbing postDataDosenPembimbing(@RequestBody DosenPembimbing dosenPembimbing) {
    return dosenPembimbingService.save(dosenPembimbing);
  }

  @PutMapping
  public DosenPembimbing editDataDosenPembimbing(@RequestBody DosenPembimbing dosenPembimbing) {
    return dosenPembimbingService.update(dosenPembimbing);
  }

  @DeleteMapping("/{id}")
  public DosenPembimbing deleteDataDosenPembimbing(@PathVariable("id") Long id) {
    return dosenPembimbingService.delete(id);
  }

}