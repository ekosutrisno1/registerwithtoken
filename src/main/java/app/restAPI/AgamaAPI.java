package app.restAPI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import app.model.master.Agama;
import app.service.data_service.AgamaService;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "rest/agama", produces = "application/json")
public class AgamaAPI {
   @Autowired
   private AgamaService agamaService;

//   @Cacheable(value = "agama-cache")
   @GetMapping
   public List<Agama> getAllAgama() {
      return agamaService.getAll();
   }

//   @Cacheable(value = "agama-cache")
   @GetMapping("/{id}")
   public Optional<Agama> getAgamaById(@PathVariable("id") Long id) {
      return agamaService.getById(id);
   }
}
