package app.restAPI;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import app.model.transaksi.Mahasiswa;
import app.service.data_service.MahasiswaService;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * MahasiswaAPI
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "rest/mahasiswa", produces = "application/json")
public class MahasiswaAPI {

  @Autowired
  private MahasiswaService mahasiswaService;

//  @Cacheable(value = "get_mahasiswa")
  @GetMapping
  public List<Mahasiswa> getAllData() {
    return mahasiswaService.getAllMahasiswa();
  }

//  @Cacheable(value = "get_mahasiswa")
  @GetMapping("/page")
  public Page<Mahasiswa> getAllDataPage(@PageableDefault(value = 10, sort = "id") Pageable pageable) {
    return mahasiswaService.getUsingPaging(pageable);
  }

//  @Cacheable(value = "get_mahasiswa", key = "#id")
  @GetMapping("/{id}")
  public Optional<Mahasiswa> getDataById(@PathVariable("id") Long id) {
    return mahasiswaService.getMahasiswaById(id);
  }

  // @CachePut(value = "get_mahasiswa", key = "id")
  @PostMapping
  public Mahasiswa postDataMahasiswa(@RequestBody Mahasiswa mahasiswa) {
    return mahasiswaService.saveMahasiswa(mahasiswa);
  }

  // @CachePut(value = "get_mahasiswa", key = "id")
  @PutMapping
  public Mahasiswa updateDataMahasiswa(@RequestBody Mahasiswa mahasiswa) {
    return mahasiswaService.updateMahasiswa(mahasiswa);
  }

  // @CacheEvict(value = "get_mahasiswa", key = "id")
  @DeleteMapping("/{id}")
  public Mahasiswa deleteDataMahasiswa(@PathVariable("id") Long id) {
    return mahasiswaService.deleteMahasiswa(id);
  }

}