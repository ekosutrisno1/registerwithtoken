package app;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import app.model.Account;
import app.service.account.AccountService;
import app.service.accout_verify.AccountVerifyService;

@SpringBootTest
class ComApplicationTests {

	@Autowired
	private AccountVerifyService accountVerifyService;

	@Autowired
	private AccountService accountService;

	@Test
	void contextLoads() {

		Account user = accountService.findByEmailOne("sutrisnoeko801@gmail.com");
		accountVerifyService.findByAccount(user.getId());

	}

}
